// alert("hi")

let studentArray = []

function addStudent(name) {
	console.log(name + " was added to the student's list.")
	studentArray.push(name);
};

function countStudents() {
	console.log("There are a total of " + studentArray.length + " students enrolled.");
};

function printStudents() {
	studentArray.sort();
	studentArray.forEach(function(name) {
		console.log(name)
	})
}

function findStudent(a) {
	answer = []
	fail = []
	studentArray.forEach(function(name) {
		if (a.toLowerCase() == name.toLowerCase()) {
			answer.push(name)
		} else if (a.toLowerCase() == name[0].toLowerCase()) {
			answer.push(name)
		} else {
			fail.push(name)
		}
	})

	if (answer.length == 1) {
		console.log(answer[0] + " is an enrollee")
	} else if (answer.length >= 2) {
		console.log(answer + " are enrollees")
	} else {
		console.log("No student found with the name " + a)
	}
}

function addSection(a) {
	let studentMap = studentArray.map(function(b) {
		return b.concat(" - section " + a); 
	})
console.log(studentMap)
}

function removeStudent(a) {
	let newName = a.charAt(0).toUpperCase() + a.slice(1)
	let currentIndex = studentArray.indexOf(newName)
	studentArray.splice(currentIndex, 1)
	console.log(a + " was removed from the student's list.")
}